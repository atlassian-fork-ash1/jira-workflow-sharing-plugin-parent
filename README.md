# Welcome to jira-workflow-sharing-plugin #

`jira-workflow-sharing-plugin` is a plugin that provides the ability to import/export workflows.

###### This is the SERVER edition of the plugin. [See the Cloud equivalent here](https://stash.atlassian.com/projects/JIRACLOUD/repos/jira-workflow-sharing-plugin-parent)

## Workflow Export ##

The export takes a workflow from JIRA and its associated custom fields and screens and puts them in a
zip file called a _workflow bundle_. This bundle can be uploaded to MPAC or to JIRA directly for import. 

It is not possible to export parts of the workflow that have configuration that reference non-exported entities (e.g.
users, groups, permission schemes, pretty much everything). For example, it is not possible to export the 
`UserInGroupCondition` because the group name will probably not make sense to anyone importing it.
The export strips the workflow of all conditions, validators and functions that are not in the
`jira-workflow-sharing-plugin/src/main/resources/whitelist.txt`. The user is told which parts of the export are removed.

A workflow bundle can be put onto MPAC for admins to install through a UI provided by this plugin. The `check-bundle.sh`
script can be used to work out if a bundle is safe to upload to MPAC. It simply checks that the bundle only contains
postfunctions, conditions and validators from `jira-workflow-sharing-plugin/src/main/resources/whitelist.txt`.

## Workflow Import ##

The import takes a bundle exported from JIRA and restores the contained workflows, fields and screens. The workflow 
bundle can be imported directly by a _system admin_ or by accessing bundles exposed by MPAC (which are safe) through
the UI provided by the plugin.

The import process will strip a _workflow bundle_ of conditions, validators and functions that are not in the
`jira-workflow-sharing-plugin/src/main/resources/whitelist.txt`.

## Use By Other Plugins ##

_Most of the interfaces are exported into OSGi so you must be careful when changing API methods._

This plugin is used by many other plugins to import workflows. For example, it is used by the workflow designer, 
project templates (and through this things like agile, service desk). It exposes its functionality through OSGi 
services exported to plugins. Some of the APIs exported include:

 - `JiraWorkflowSharingExporter`
 - `JiraWorkflowSharingImporter`
 - ...

## Contributing ##

1. [Raise an issue](https://ecosystem.atlassian.net/browse/JWSP).
2. Start the issue by writing testing notes. Pair with a QA to validate them and catch gaps.
3. Branch off master, follow the `issue/<ISSUE_KEY>-<SOME_DESCRIPTION>` convention (never commit to master, only merge).
4. Ensure Bamboo [builds of your branch](https://server-gdn-bamboo.internal.atlassian.com/browse/PLUGINS-JWSP) are green. You
  can run the tests locally with

        $ mvn3 verify # All tests
        $ mvn3 test # Unit Tests
        $ mvn3 integration-test -DtestGroups=wired # Wired tests
        $ mvn3 integration-test -DtestGroups=webdriver # Web Driver Tests

5. Ensure you've executed your testing notes.
6. Raise a Pull Request for review -- add @BrendenBain
7. Upon approval, merge the pull request to master & delete the branch.

## Releasing ##

Standard mvn3 release will work:

    $ mvn3 release:prepare [-Dxvfb.enable=true]
    $ mvn3 release:perform [-Dxvfb.enable=true]

The `-Dxvfb.enable=true` can be used to release from a box without a screen.

## Tests ##

There are three types of tests in the plugin:

1. _Unit Tests_: Can be run directly in your IDE or by using 

        $ mvn3 -B clean test

2. _Wired Tests_: Install a testing plugin into a running JIRA and execute the contained tests. See
https://developer.atlassian.com/x/5QHq for details.
 
        $ mvn3 -B clean integration-test -DtestGroups=wired 

3. _Webdriver Tests_: 

        $ mvn3 -B clean integration-test -DtestGroups=webdriver 

### Wired Tests ###

Running a wired test is a PITA. Here is what I did:
        
        $ mvn3 clean install -DskipTests
        $ cd jira-workflow-sharing-plugin
        $ mvn3 jira:debug or atlas-debug (provided you have version >= 5.0.2 of AMPS)
        $ curl http://localhost:2990/jira/rest/atlassiantestrunner/1.0/runtest/<TEST_FULL_JAVA_NAME> 

## Getting started ##
   
    $ git clone git@bitbucket.org:atlassian/jira-workflow-sharing-plugin-parent.git
    $ cd jira-workflow-sharing-plugin-parent
    $ mvn3 clean test

## Run plugin in JIRA ##

    $ cd jira-workflow-sharing-plugin-parent
    $ mvn3 jira:debug or atlas-debug (provided you have version >= 5.0.2 of AMPS)

## Branches ##

master - JIRA 8.0+ 
7_x_branch - JIRA 7.0+ (no CI builds configured for this branch)
6_x_branch - JIRA 6.3+

## Links ##

- Issues - https://ecosystem.atlassian.net/browse/JWSP/
- Builds - https://server-gdn-bamboo.internal.atlassian.com/browse/PLUGINS-JWSP
