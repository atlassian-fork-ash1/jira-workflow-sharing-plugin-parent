package com.atlassian.jira.plugins.workflow.sharing;

public interface WorkflowLayoutKeyFinder
{
    String getActiveLayoutKey(String workflowName);

    String getDraftLayoutKey(String workflowName);

    String getActiveLayoutV2Key(String workflowName);

    String getDraftLayoutV2Key(String workflowName);
}
