package com.atlassian.jira.plugins.workflow.sharing.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class ScreenTabInfo implements Serializable
{
    private static final long serialVersionUID = -3175760095242128840L;
    private final Long originalId;
    private final String name;
    private final Integer position;
    private final List<ScreenItemInfo> items;

    @JsonCreator
    public ScreenTabInfo(@JsonProperty("originalId") Long originalId
            ,@JsonProperty("name") String name
            ,@JsonProperty("position") Integer position
            ,@JsonProperty("items") List<ScreenItemInfo> items)
    {
        this.originalId = originalId;
        this.name = name;
        this.position = position;
        this.items = items;
    }

    public Long getOriginalId()
    {
        return originalId;
    }

    public String getName()
    {
        return name;
    }

    public Integer getPosition()
    {
        return position;
    }

    public List<ScreenItemInfo> getItems()
    {
        return items;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(originalId).append(name).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this) { return true; }

        if(!(obj instanceof ScreenTabInfo)) { return false; }

        ScreenTabInfo info = (ScreenTabInfo) obj;

        return new EqualsBuilder().append(this.getOriginalId(),info.getOriginalId()).append(this.getName(), info.getName()).isEquals();
    }

    @Override
    public String toString()
    {
        return "ScreenTabInfo name:" + name;//NON-NLS
    }
}
