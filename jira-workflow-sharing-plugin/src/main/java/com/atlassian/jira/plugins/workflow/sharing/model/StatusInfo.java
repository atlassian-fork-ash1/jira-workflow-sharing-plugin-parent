package com.atlassian.jira.plugins.workflow.sharing.model;

import com.google.common.base.Function;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.type.TypeReference;

import java.io.Serializable;
import java.util.List;

/**
 * @since v6.2
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatusInfo implements Serializable
{
    public static TypeReference<List<StatusInfo>> LIST_TYPE = new TypeReference<List<StatusInfo>>(){};

    private static final long serialVersionUID = 6812345556333759499L;
    private final String originalId;
    private final String name;
    private final String description;
    private final Long statusCategoryId;

    @JsonCreator
    public StatusInfo(
            @JsonProperty("originalId") String originalId,
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("statusCategoryId") Long statusCategoryId)
    {
        this.originalId = originalId;
        this.name = name;
        this.description = description;
        this.statusCategoryId = statusCategoryId;
    }

    public Long getStatusCategoryId()
    {
        return statusCategoryId;
    }

    public String getDescription()
    {
        return description;
    }

    public String getName()
    {
        return name;
    }

    public String getOriginalId()
    {
        return originalId;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this) { return true; }

        if(!(obj instanceof StatusInfo)) { return false; }

        StatusInfo info = (StatusInfo) obj;

        return new EqualsBuilder().append(this.getOriginalId(),info.getOriginalId()).append(this.getName(), info.getName()).isEquals();
    }

    @Override
    public int hashCode()
    {
        int result = originalId.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
