package com.atlassian.jira.plugins.workflow.sharing.util;

import com.atlassian.marketplace.client.HttpConfiguration;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.BasicClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;

import static org.apache.http.client.params.ClientPNames.COOKIE_POLICY;
import static org.apache.http.client.params.CookiePolicy.IGNORE_COOKIES;
import static org.apache.http.conn.params.ConnRoutePNames.DEFAULT_PROXY;
import static org.apache.http.params.CoreConnectionPNames.CONNECTION_TIMEOUT;
import static org.apache.http.params.CoreConnectionPNames.SO_TIMEOUT;

public class HttpUtils
{
    public static HttpResult execute(HttpRequestBase method) throws IOException
    {
        ClientConnectionManager httpConnectionManager = new BasicClientConnectionManager();

        HttpParams httpParams = new BasicHttpParams();
        httpParams.setIntParameter(CONNECTION_TIMEOUT, 60000);
        httpParams.setIntParameter(SO_TIMEOUT, 60000);
        httpParams.setParameter(COOKIE_POLICY, IGNORE_COOKIES);

        DefaultHttpClient httpClient = new DefaultHttpClient(httpConnectionManager, httpParams);

        configureProxy(httpClient);

        return new HttpResult(httpClient.execute(method), httpConnectionManager, method);
    }

    private static void configureProxy(DefaultHttpClient httpClient)
    {
        String host = System.getProperty("http.proxyHost");
        if (host != null)
        {
            Integer port = Integer.getInteger("http.proxyPort", 80);

            httpClient.getParams().setParameter(DEFAULT_PROXY, new HttpHost(host, port));

            String proxyUser = System.getProperty("http.proxyUser");

            if (proxyUser != null)
            {
                String proxyPassword = System.getProperty("http.proxyPassword");
                String method = System.getProperty("http.proxyAuth", "basic");
                String domain = System.getProperty("http.proxyNtlmDomain", "");
                String workstation = System.getProperty("http.proxyNtlmWorkstation", "");

                AuthScope proxyAuthScope = new AuthScope(host, port);
                Credentials proxyCredentials;
                switch (HttpConfiguration.ProxyConfiguration.AuthMethod.valueOf(method.trim().toUpperCase()))
                {
                    case NTLM:
                        proxyCredentials = new NTCredentials(proxyUser, proxyPassword,
                                (workstation == null) ? "" : workstation,
                                (domain == null) ? "" : domain);
                        break;

                    default:
                        proxyCredentials = new UsernamePasswordCredentials(proxyUser, proxyPassword);
                        break;
                }
                httpClient.getCredentialsProvider().setCredentials(proxyAuthScope, proxyCredentials);
            }
        }
    }
}
