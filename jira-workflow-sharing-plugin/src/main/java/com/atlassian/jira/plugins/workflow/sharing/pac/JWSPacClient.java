package com.atlassian.jira.plugins.workflow.sharing.pac;

import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.model.Plugin;
import com.atlassian.marketplace.client.model.PluginSummary;
import com.atlassian.upm.api.util.Option;

public interface JWSPacClient
{
    Page<PluginSummary> getWorkflowBundleList(int offset);

    Page<PluginSummary> getWorkflowBundleListByFilter(Option<String> filter, int offset);

    Plugin getWorkflowBundleDetails(String pluginKey);
}
