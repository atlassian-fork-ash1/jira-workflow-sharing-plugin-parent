package com.atlassian.jira.plugins.workflow.sharing.servlet;

public enum ServletMapping
{
    START_EXPORT("wfshare-export", "exporter/start-export.vm")//NON-NLS
    ,EXPORT_ADD_NOTES("add-notes", "exporter/export-add-notes.vm")//NON-NLS
    ,EXPORT_WORKFLOW("export-workflow", "")//NON-NLS
    ,EXPORT_WORKFLOW_SUCCESS("export-workflow-success", "exporter/export-success.vm")//NON-NLS
    ,IMPORT_CHOOSE_ZIP("wfshare-import", "importer/import-choose-zip.vm")//NON-NLS
    ,IMPORT_SET_NAME("set-workflow-name", "importer/import-choose-name.vm")//NON-NLS
    ,IMPORT_MAP_STATUSES("map-statuses", "importer/import-map-statuses.vm")//NON-NLS
    ,IMPORT_VIEW_SUMMARY("summary", "importer/import-summary.vm")//NON-NLS
    ,IMPORT_WORKFLOW("import-workflow", "")//NON-NLS
    ,IMPORT_WORKFLOW_SUCCESS("import-workflow-success", "importer/import-success.vm")//NON-NLS
    ,EXPORT_ERROR("", "export-error.vm")//NON-NLS
    ,IMPORT_ERROR("", "import-error.vm")//NON-NLS
    ,NOT_MAPPED("", "");

    public static final String TEMPLATE_PREFIX = "/templates/workflow-sharing/";//NON-NLS

    private String path;
    private String resultTemplate;

    private ServletMapping(String path, String template)
    {
        this.path = path;
        this.resultTemplate = TEMPLATE_PREFIX + template;
    }

    public String getPath()
    {
        return path;
    }

    public String getResultTemplate()
    {
        return resultTemplate;
    }

    public ServletMapping previous()
    {
        if(this.ordinal() > 0)
        {
            return ServletMapping.values()[this.ordinal() - 1];
        }
        else
        {
            return null;
        }
    }

    public static ServletMapping fromPath(String path)
    {
        for (ServletMapping mapping : ServletMapping.values())
        {
            if (mapping.getPath().equals(path))
            {
                return mapping;
            }
        }

        return NOT_MAPPED;
    }
}
