package com.atlassian.jira.plugins.workflow.sharing.importer;

import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugins.workflow.sharing.ImportableJiraWorkflow;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowExtensionsHelper;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowLayoutKeyFinder;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowScreensHelper;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.CustomFieldCreator;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.ScreenCreator;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.WorkflowStatusHelper;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.sysbliss.jira.plugins.workflow.WorkflowDesignerConstants;
import com.sysbliss.jira.plugins.workflow.util.WorkflowDesignerPropertySet;

import java.util.*;

public class JiraWorkflowSharingImporterImpl implements JiraWorkflowSharingImporter
{
    private static final String JIRA_STATUS_ID = "jira.status.id";
    private static final String NEW_STATUS_DEFAULT_ICON = "/images/icons/status_generic.gif";
    private final WorkflowStatusHelper workflowStatusHelper;
    private final WorkflowManager workflowManager;
    private final WorkflowExtensionsHelper workflowExtensionsHelper;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final WorkflowDesignerPropertySet workflowDesignerPropertySet;
    private final CustomFieldCreator customFieldCreator;
    private final ScreenCreator screenCreator;
    private final WorkflowScreensHelper workflowScreensHelper;
    private final I18nResolver i18n;
    private final WorkflowLayoutKeyFinder workflowLayoutKeyFinder;
    private final PluginAccessor pluginAccessor;

    public JiraWorkflowSharingImporterImpl(WorkflowStatusHelper workflowStatusHelper, WorkflowManager workflowManager,
            WorkflowExtensionsHelper workflowExtensionsHelper, JiraAuthenticationContext jiraAuthenticationContext,
            WorkflowDesignerPropertySet workflowDesignerPropertySet, CustomFieldCreator customFieldCreator,
            ScreenCreator screenCreator, WorkflowScreensHelper workflowScreensHelper,
            I18nResolver i18n, WorkflowLayoutKeyFinder workflowLayoutKeyFinder, PluginAccessor pluginAccessor)
    {
        this.workflowStatusHelper = workflowStatusHelper;
        this.workflowManager = workflowManager;
        this.workflowExtensionsHelper = workflowExtensionsHelper;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.workflowDesignerPropertySet = workflowDesignerPropertySet;
        this.customFieldCreator = customFieldCreator;
        this.screenCreator = screenCreator;
        this.workflowScreensHelper = workflowScreensHelper;
        this.i18n = i18n;
        this.workflowLayoutKeyFinder = workflowLayoutKeyFinder;
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public JiraWorkflow importWorkflow(SharedWorkflowImportPlan plan)
    {
        ImportableJiraWorkflow jiraWorkflow;
        Set<Status> createdStatuses = Collections.emptySet();
        Set<CustomField> createdCustomFields = Collections.emptySet();
        Set<FieldScreen> createdScreens = Collections.emptySet();

        try
        {
            jiraWorkflow = getJiraWorkflowFromPlan(plan);

            createdStatuses = importStatuses(plan, jiraWorkflow);

            workflowExtensionsHelper.removeIllegalComponents(jiraWorkflow);

            CustomFieldCreationResult customFieldCreationResult = importCustomFields(plan);
            createdCustomFields = customFieldCreationResult.getCreatedFields();

            ScreenCreationResult screenCreationResult = importScreens(plan, customFieldCreationResult.getOldToNewIdMapping());
            createdScreens = screenCreationResult.getCreatedScreens();

            updateWorkflowScreenIds(jiraWorkflow, screenCreationResult.getOldToNewIdMapping());

            createWorkflow(jiraWorkflow);

            //save the layout
            importLayoutAndAnnotations(plan);
        }
        catch (Exception e)
        {
            rollback(createdStatuses, createdCustomFields, createdScreens);

            throw new RuntimeException(e.getMessage(), e);
        }

        return jiraWorkflow;
    }

    private ImportableJiraWorkflow getJiraWorkflowFromPlan(SharedWorkflowImportPlan plan) throws Exception
    {
        try
        {
            WorkflowDescriptor descriptor = WorkflowUtil.convertXMLtoWorkflowDescriptor(plan.getWorkflowXml());
            return new ImportableJiraWorkflow(plan.getWorkflowName(), descriptor, workflowManager);
        }
        catch (Exception e)
        {
            throw new Exception(i18n.getText("wfshare.exception.unable.to.create.workflow.descriptor"), e);
        }
    }

    private Set<Status> importStatuses(SharedWorkflowImportPlan plan, ImportableJiraWorkflow jiraWorkflow)
            throws Exception
    {
        Set<Status> createdStatuses = new HashSet<Status>();
        if (null != plan.getStatusMappings() && !plan.getStatusMappings().isEmpty())
        {
            try
            {
                createdStatuses = updateStatuses(jiraWorkflow, plan.getStatusMappings());
            }
            catch (Exception e)
            {
                throw new Exception(i18n.getText("wfshare.exception.unable.to.migrate.statuses"), e);
            }
        }

        return createdStatuses;
    }

    private CustomFieldCreationResult importCustomFields(SharedWorkflowImportPlan plan) throws Exception
    {
        List<CustomFieldInfo> fieldInfoList;
        try
        {
            fieldInfoList = plan.getAllowedEnabledCustomFields(pluginAccessor);
        }
        catch (Exception e)
        {
            throw new Exception(i18n.getText("wfshare.exception.unable.to.read.custom.field.data"), e);
        }

        CustomFieldCreationResult result = new CustomFieldCreationResult();

        if (fieldInfoList != null && !fieldInfoList.isEmpty())
        {
            try
            {
                result = createCustomFields(fieldInfoList);
            }
            catch (Exception e)
            {
                throw new Exception(i18n.getText("wfshare.exception.unable.to.migrate.custom.fields"), e);
            }
        }

        return result;
    }

    private ScreenCreationResult importScreens(SharedWorkflowImportPlan plan, Map<String, String> fieldMappings)
            throws Exception
    {
        Iterable<ScreenInfo> screenInfoList;
        try
        {
            screenInfoList = plan.getScreenInfo();
        }
        catch (Exception e)
        {
            throw new Exception(i18n.getText("wfshare.exception.unable.to.read.screen.data"), e);
        }

        try
        {
            return createScreens(screenInfoList, fieldMappings);
        }
        catch (Exception e)
        {
            throw new Exception(i18n.getText("wfshare.exception.unable.to.migrate.screens"), e);
        }
    }

    private void updateWorkflowScreenIds(ImportableJiraWorkflow jiraWorkflow, Map<Long, Long> oldToNewIdMapping)
    {
        workflowScreensHelper.updateWorkflowScreenIds(jiraWorkflow, oldToNewIdMapping);
        jiraWorkflow.resetFieldScreens();
    }

    private void createWorkflow(ImportableJiraWorkflow jiraWorkflow) throws Exception
    {
        try
        {
            workflowManager.createWorkflow(jiraAuthenticationContext.getUser(), jiraWorkflow);
        }
        catch (Exception e)
        {
            throw new Exception(i18n.getText("wfshare.exception.unable.to.create.workflow"), e);
        }
    }

    private void importLayoutAndAnnotations(SharedWorkflowImportPlan plan)
    {
        try
        {
            if (plan.getLayout() != null)
            {
                String layoutPropKey = workflowLayoutKeyFinder.getActiveLayoutKey(plan.getWorkflowName());
                workflowDesignerPropertySet.setProperty(layoutPropKey, plan.getLayout());
            }

            if (plan.getLayoutV2() != null)
            {
                String layoutV2PropKey = workflowLayoutKeyFinder.getActiveLayoutV2Key(plan.getWorkflowName());
                workflowDesignerPropertySet.setProperty(layoutV2PropKey, plan.getLayoutV2());
            }

            if (plan.getAnnotations() != null)
            {
                String annotationPropKey = WorkflowDesignerConstants.ANNOTATION_PREFIX.concat(plan.getWorkflowName());
                workflowDesignerPropertySet.setProperty(annotationPropKey, plan.getAnnotations());
            }
        }
        catch (Exception e)
        {
            //just ignore, no biggie if layout and annotations are missing
        }
    }

    private void rollback(Set<Status> createdStatuses, Set<CustomField> createdCustomFields, Set<FieldScreen> createdScreens)
    {
        removeStatuses(createdStatuses);
        removeCustomFields(createdCustomFields);
        removeScreens(createdScreens);
    }

    private void removeStatuses(Set<Status> statuses)
    {
        for (Status status : statuses)
        {
            try
            {
                workflowStatusHelper.removeStatus(status.getId());
            }
            catch (Exception e)
            {
                //ignore
            }
        }
    }

    private Set<Status> updateStatuses(ImportableJiraWorkflow jiraWorkflow, List<StatusMapping> statusMappings)
            throws Exception
    {
        List<StepDescriptor> steps = jiraWorkflow.getDescriptor().getSteps();
        Set<Status> createdStatuses = Sets.newHashSet();
        Map<String, StepDescriptor> statusStepMap = Maps.uniqueIndex(steps, new Function<StepDescriptor, String>()
        {
            @Override
            public String apply(StepDescriptor stepDescriptor)
            {
                return (String) stepDescriptor.getMetaAttributes().get(JIRA_STATUS_ID);
            }
        });

        for (StatusMapping mapping : statusMappings)
        {
            if (mapping.getNewId().equals("-1"))
            {
                try
                {
                    Status status = workflowStatusHelper.createStatus(mapping, NEW_STATUS_DEFAULT_ICON);
                    mapping.setNewId(status.getId());
                    createdStatuses.add(status);
                }
                catch (Exception e)
                {
                    if (!createdStatuses.isEmpty())
                    {
                        removeStatuses(createdStatuses);
                    }

                    throw new Exception(e);
                }
            }

            StepDescriptor step = statusStepMap.get(mapping.getOriginalId());
            if (null != step)
            {
                step.getMetaAttributes().put(JIRA_STATUS_ID, mapping.getNewId());
            }
        }

        return createdStatuses;
    }

    private CustomFieldCreationResult createCustomFields(List<CustomFieldInfo> fieldInfoList) throws Exception
    {
        CustomFieldCreationResult result = new CustomFieldCreationResult();

        for (CustomFieldInfo fieldInfo : fieldInfoList)
        {
            try
            {
                CustomField cf = customFieldCreator.createCustomField(fieldInfo);
                result.addCustomField(cf);
                result.addMapping(fieldInfo.getOriginalId(), cf.getId());
            }
            catch (Exception e)
            {
                if (!result.getCreatedFields().isEmpty())
                {
                    removeCustomFields(result.getCreatedFields());
                }

                throw new Exception(e);
            }
        }

        return result;
    }

    private ScreenCreationResult createScreens(Iterable<ScreenInfo> screenInfoList, Map<String, String> createdFieldsMapping)
            throws Exception
    {
        ScreenCreationResult result = new ScreenCreationResult();

        for (ScreenInfo screenInfo : screenInfoList)
        {
            try
            {
                FieldScreen screen = screenCreator.createScreen(screenInfo);

                result.addScreen(screen);
                result.addMapping(screenInfo.getOriginalId(), screen.getId());

                screenCreator.addScreenTabs(screen, screenInfo, createdFieldsMapping);
            }
            catch (Exception e)
            {
                if (!result.getCreatedScreens().isEmpty())
                {
                    removeScreens(result.getCreatedScreens());
                }
                throw new Exception(e);

            }
        }

        return result;
    }

    private void removeCustomFields(Set<CustomField> createdFields)
    {
        for (CustomField field : createdFields)
        {
            try
            {
                customFieldCreator.removeCustomFieldAndSchemes(field);
            }
            catch (RemoveException e)
            {
                //ignore
            }
        }
    }

    private void removeScreens(Set<FieldScreen> createdScreens)
    {
        for (FieldScreen screen : createdScreens)
        {
            screenCreator.removeScreen(screen.getId());
        }
    }

    private class ScreenCreationResult
    {

        private Set<FieldScreen> createdScreens;
        private Map<Long, Long> oldToNewIdMapping;

        private ScreenCreationResult()
        {
            this.createdScreens = new HashSet<FieldScreen>();
            this.oldToNewIdMapping = new HashMap<Long, Long>();
        }

        public void addScreen(FieldScreen screen)
        {
            this.createdScreens.add(screen);
        }

        public void addMapping(Long oldId, Long newId)
        {
            this.oldToNewIdMapping.put(oldId, newId);
        }

        public Set<FieldScreen> getCreatedScreens()
        {
            return createdScreens;
        }

        public Map<Long, Long> getOldToNewIdMapping()
        {
            return oldToNewIdMapping;
        }
    }

    private class CustomFieldCreationResult
    {
        private Set<CustomField> createdFields;
        private Map<String, String> oldToNewIdMapping;

        private CustomFieldCreationResult()
        {
            this.createdFields = new HashSet<CustomField>();
            this.oldToNewIdMapping = new HashMap<String, String>();
        }

        public void addCustomField(CustomField field)
        {
            this.createdFields.add(field);
        }

        public void addMapping(String oldId, String newId)
        {
            this.oldToNewIdMapping.put(oldId, newId);
        }

        public Set<CustomField> getCreatedFields()
        {
            return createdFields;
        }

        public Map<String, String> getOldToNewIdMapping()
        {
            return oldToNewIdMapping;
        }
    }
}
