package com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenItemInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenTabInfo;
import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

@ExportAsService
@Component
public class WorkflowScreensHelperImpl implements WorkflowScreensHelper
{
    private final FieldScreenManager fieldScreenManager;
    private final WorkflowCustomFieldsHelper customFieldsHelper;

    @Autowired
    public WorkflowScreensHelperImpl(FieldScreenManager fieldScreenManager, WorkflowCustomFieldsHelper customFieldsHelper)
    {
        this.fieldScreenManager = fieldScreenManager;
        this.customFieldsHelper = customFieldsHelper;
    }

    @Override
    public Set<String> getClassnamesForScreen(String screenId)
    {
        Set<String> classNames = new HashSet<String>();
        if(StringUtils.isNumeric(screenId))
        {
            FieldScreen screen = fieldScreenManager.getFieldScreen(Long.parseLong(screenId));

            for(FieldScreenTab tab : screen.getTabs())
            {
                List<FieldScreenLayoutItem> items = tab.getFieldScreenLayoutItems();

                for(FieldScreenLayoutItem item : items)
                {
                    String cfClassname = customFieldsHelper.getCustomFieldTypeClassname(item.getFieldId());
                    if(StringUtils.isNotBlank(cfClassname))
                    {
                        classNames.add(cfClassname);
                    }
                }
            }
        }

        return classNames;
    }

    @Override
    public Set<String> getClassnamesForScreenFromAction(ActionDescriptor action)
    {
        Set<String> classNames = new HashSet<String>();
        
        if(FIELDSCREEN_VIEW.equals(action.getView()) && action.getMetaAttributes().containsKey(WorkflowScreensHelper.FIELDSCREEN_ID_NAME))
        {
            String screenId = (String) action.getMetaAttributes().get(FIELDSCREEN_ID_NAME);
            classNames.addAll(getClassnamesForScreen(screenId));
        }
        
        return classNames;
    }

    @Override
    public Set<String> getPluginRelatedClassnamesForScreenFromAction(ActionDescriptor action)
    {
        Set<String> classNames = Sets.newHashSet();

        if (FIELDSCREEN_VIEW.equals(action.getView()) && action.getMetaAttributes().containsKey(WorkflowScreensHelper.FIELDSCREEN_ID_NAME))
        {
            String screenId = (String) action.getMetaAttributes().get(FIELDSCREEN_ID_NAME);

            if (StringUtils.isNumeric(screenId))
            {
                FieldScreen screen = fieldScreenManager.getFieldScreen(Long.parseLong(screenId));

                for (FieldScreenTab tab : screen.getTabs())
                {
                    List<FieldScreenLayoutItem> items = tab.getFieldScreenLayoutItems();

                    for (FieldScreenLayoutItem item : items)
                    {
                        String fieldId = item.getFieldId();
                        String className = customFieldsHelper.getPluginRelatedClassName(fieldId);

                        if (className != null)
                        {
                            classNames.add(className);
                        }
                    }
                }
            }
        }

        return classNames;
    }

    @Override
    public Iterable<String> getCustomFieldIdsForWorkflowScreens(JiraWorkflow workflow)
    {
        Set<String> ids = new HashSet<String>();

        Collection<ActionDescriptor> allActions = workflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            if(FIELDSCREEN_VIEW.equals(action.getView()) && action.getMetaAttributes().containsKey(FIELDSCREEN_ID_NAME))
            {
                String screenId = (String) action.getMetaAttributes().get(FIELDSCREEN_ID_NAME);

                if(StringUtils.isNumeric(screenId))
                {
                    FieldScreen screen = fieldScreenManager.getFieldScreen(Long.parseLong(screenId));

                    for(FieldScreenTab tab : screen.getTabs())
                    {
                        List<FieldScreenLayoutItem> items = tab.getFieldScreenLayoutItems();

                        for(FieldScreenLayoutItem item : items)
                        {
                            ids.add(item.getFieldId());
                        }
                    }
                }
            }
        }
        
        return ids;
    }

    @Override
    public void updateWorkflowScreenIds(ConfigurableJiraWorkflow jiraWorkflow, Map<Long, Long> oldToNewIdMapping)
    {
        Collection<ActionDescriptor> allActions = jiraWorkflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            if(FIELDSCREEN_VIEW.equals(action.getView()) && action.getMetaAttributes().containsKey(FIELDSCREEN_ID_NAME))
            {
                String screenId = (String) action.getMetaAttributes().get(FIELDSCREEN_ID_NAME);

                if(StringUtils.isNumeric(screenId))
                {
                    Long oldScreenId = Long.parseLong(screenId);
                    if(oldToNewIdMapping.containsKey(oldScreenId))
                    {
                        action.getMetaAttributes().put(FIELDSCREEN_ID_NAME,Long.toString(oldToNewIdMapping.get(oldScreenId)));
                    }
                }
            }
        }
    }

    @Override
    public String getScreensJson(JiraWorkflow workflow) throws IOException
    {
        List<ScreenInfo> screenInfoList = new ArrayList<ScreenInfo>();
        
        Collection<ActionDescriptor> allActions = workflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            if(FIELDSCREEN_VIEW.equals(action.getView()) && action.getMetaAttributes().containsKey(FIELDSCREEN_ID_NAME))
            {
                final String screenId = (String) action.getMetaAttributes().get(FIELDSCREEN_ID_NAME);

                if(StringUtils.isNumeric(screenId) && Collections2.filter(screenInfoList, new Predicate<ScreenInfo>()
                {
                    @Override
                    public boolean apply(ScreenInfo input)
                    {
                        return input.getOriginalId().toString().equals(screenId);
                    }
                }).isEmpty())
                {
                    FieldScreen screen = fieldScreenManager.getFieldScreen(Long.parseLong(screenId));
                    
                    List<ScreenTabInfo> tabs = new ArrayList<ScreenTabInfo>(screen.getTabs().size());
                    
                    for(FieldScreenTab tab : screen.getTabs())
                    {
                        List<FieldScreenLayoutItem> items = new ArrayList<FieldScreenLayoutItem>(tab.getFieldScreenLayoutItems());

                        Collections.sort(items, new PositionComparator());
                        
                        List<ScreenItemInfo> tabItems = new ArrayList<ScreenItemInfo>(items.size());

                        int position = 0;
                        for(FieldScreenLayoutItem item : items)
                        {
                            String fieldId = item.getFieldId();
                            if (!customFieldsHelper.isFromPlugin(fieldId))
                            {
                                tabItems.add(new ScreenItemInfo(item.getId(), fieldId, position));
                                position++;
                            }
                        }
                        
                        tabs.add(new ScreenTabInfo(tab.getId(), tab.getName(), tab.getPosition(), tabItems));
                    }
                    
                    screenInfoList.add(new ScreenInfo(screen.getId(),screen.getName(),screen.getDescription(),tabs));
                }
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        final StringWriter sw = new StringWriter();

        mapper.writeValue(sw,screenInfoList);

        return sw.toString();
    }


    private static class PositionComparator implements Comparator<FieldScreenLayoutItem>
    {
        @Override
        public int compare(FieldScreenLayoutItem first, FieldScreenLayoutItem second)
        {
            return new CompareToBuilder()
                    .append(first.getPosition(), second.getPosition())
                    .toComparison();
        }
    }
}
