package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.issue.status.SimpleStatus;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugins.workflow.sharing.importer.StatusMapping;
import com.atlassian.jira.plugins.workflow.sharing.model.StatusInfo;

import java.util.List;
import java.util.Map;

public interface WorkflowStatusHelper
{
    /**
     * @deprecated use {@link #getStatusHolders(String, java.util.List)} instead.
     */
    Map<String,StatusMapping> getStatusHolders(String workflowXml);

    Map<String,StatusMapping> getStatusHolders(String workflowXml, List<StatusInfo> statusInfo);

    List<Status> getJiraStatuses();

    void removeStatus(String id) throws Exception;

    /**
     * @deprecated use {@link #createStatus(StatusMapping, String)} instead.
     */
    @Deprecated
    Status createStatus(String newName, String newStatusDefaultIcon) throws Exception;

    Status createStatus(StatusMapping mapping, String newStatusDefaultIcon) throws Exception;

    String getNameForStatusId(String id);

    String getValidStatusName(String newName);

    SimpleStatus getSimpleStatusFromMapping(StatusMapping statusMapping);
}
