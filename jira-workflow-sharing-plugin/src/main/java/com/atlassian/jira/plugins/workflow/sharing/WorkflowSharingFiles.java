package com.atlassian.jira.plugins.workflow.sharing;

public enum WorkflowSharingFiles {
    WORKFLOW("workflow.xml")//NON-NLS
    ,LAYOUT("layout.json")//NON-NLS
    ,LAYOUT_V2("layout.v2.json")//NON-NLS
    ,ANNOTATION("annotation.json")//NON-NLS
    ,PLUGINS("plugins.json")//NON-NLS
    ,PLUGINS_DIR("plugins/")//NON-NLS
    ,CUSTOM_FIELDS("customfields.json")//NON-NLS
    ,STATUSES("statuses.json")//NON-NLS
    ,SCREENS("screens.json")//NON-NLS
    ,NOTES("notes.md");//NON-NLS

    private String path;

    WorkflowSharingFiles(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
