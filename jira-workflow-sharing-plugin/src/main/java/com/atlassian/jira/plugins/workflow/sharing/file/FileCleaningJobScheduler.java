package com.atlassian.jira.plugins.workflow.sharing.file;

/**
 * The File Cleaning Scheduler interface
 *
 */
public interface FileCleaningJobScheduler
{
    void start();
    void stop();

    /**
     * Get the maximum age of an export in milliseconds.
     *
     * @return the maximum age of an export in milliseconds.
     */
    long getCurrentMaxAge();
}
