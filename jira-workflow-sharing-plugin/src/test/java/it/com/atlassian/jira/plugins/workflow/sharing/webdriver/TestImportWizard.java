package it.com.atlassian.jira.plugins.workflow.sharing.webdriver;

import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.pages.admin.workflow.WorkflowsPage;
import com.atlassian.pageobjects.elements.Option;
import com.atlassian.pageobjects.elements.Options;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.Test;
import ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects.*;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.ImportTestUtils;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.TestConstants;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

@WebTest({ Category.WEBDRIVER_TEST, Category.ADMINISTRATION, Category.WORKFLOW })
@RestoreOnce("blankprojects.zip")
public class TestImportWizard extends JwspWebDriverTestCase
{
    @Test
    public void nonAdminCannotAccessImport() throws UnsupportedEncodingException
    {
        nonAdminCannotAccess(SelectWorkflow.URL);
    }

    @Test
    public void nonSysAdminCannotUploadFromComputer()
    {
        SelectWorkflow selectWorkflow = jira.quickLogin("jiraadmin", "jiraadmin", SelectWorkflow.class);

        assertTrue(selectWorkflow.isModesNotPresent());
        assertFalse(selectWorkflow.isNextButtonPresent());
        assertFalse(selectWorkflow.isBackButtonPresent());

        assertCancel(selectWorkflow);
    }

    @Test
    public void testImport() throws IOException
    {
        ChooseName chooseName = assertWorkflowPageAsSysadmin();
        MapStatuses mapStatuses = assertChooseName(chooseName);
        Summary summary = assertMapStatuses(mapStatuses);
        WorkflowImported workflowImported = assertSummary(summary);
        assertWorkflowImported(workflowImported);
    }

    @Test
    public void testUpmWorkflowImg() {
        final SelectWorkflow page = loginAsSysAdminAndGoToImport();
        //This test requires any workflow with an image to be found in marketplace. The below string is th name of
        //a workflow managed by atlassian that contains image. The test is performed on it.
        final String workflowNameToFind = "Change Management for Jira Service Desk";

        final SelectWorkflow.WorkflowSearchResultView searchResult = page.findWorkflowInMarketplace(workflowNameToFind);

        assertTrue(searchResult.isPreviewAnchorVisible());
        assertFalse(searchResult.isLightBoxAnchorVisible());

        searchResult.clickPreview();

        assertTrue(searchResult.isLightBoxAnchorVisible());

        searchResult.clickLightBox();

        assertTrue(searchResult.isPreviewAnchorVisible());
        assertFalse(searchResult.isLightBoxAnchorVisible());
    }

    private SelectWorkflow loginAsSysAdminAndGoToImport()
    {
        return jira.quickLoginAsSysadmin(SelectWorkflow.class);
    }

    private ChooseName assertWorkflowPageAsSysadmin()
    {
        SelectWorkflow page = loginAsSysAdminAndGoToImport();

        assertCancel(page);
        assertFalse(page.isBackButtonPresent());
        assertTrue(page.isNextButtonPresent());

        assertTrue(page.isMarketplaceSelected());

        page.selectMarketplace();
        assertTrue(page.isMarketplaceContainerVisible());
        assertTrue(page.isNextButtonInvisible());
        assertTrue(page.isFileUploadInvisible());

        ChooseName chooseName = uploadBundleAndContinue(page);
        assertCurrentURL(chooseName.getUrl());

        return chooseName;
    }

    private MapStatuses assertChooseName(ChooseName page)
    {
        assertButtons(page);
        assertEquals("Screensfieldsnotesworkflow", page.getName());

        SelectWorkflow selectWorkflow = page.back();
        page = uploadBundleAndContinue(selectWorkflow);
        assertEquals("Screensfieldsnotesworkflow", page.getName());

        selectWorkflow = page.browserBack(1);
        page = selectWorkflow.browserForward(1);
        assertEquals("Screensfieldsnotesworkflow", page.getName());

        assertInvalidWorkflowName(page, " ", "You must specify a workflow name.");
        assertInvalidWorkflowName(page, Character.toString((char) 128), "Please use only ASCII characters for the workflow name.");
        assertInvalidWorkflowName(page, "With trailing whitespace ", "Workflow name cannot contain leading or trailing whitespaces.");

        page.setName("My Workflow");
        MapStatuses mapStatuses = page.next();
        assertCurrentURL(mapStatuses.getUrl());

        return mapStatuses;
    }

    private void assertInvalidWorkflowName(ChooseName page, String name, String expectedErrorMessage)
    {
        page.setName(name);
        page.next();
        assertCurrentURL(page.getUrl());
        assertTrue(page.isErrorMessageShown());
        assertEquals(expectedErrorMessage, page.getMessageParagraph());
    }

    private ChooseName uploadBundleAndContinue(SelectWorkflow page)
    {
        return uploadBundleAndContinue(page, TestConstants.SCREENS_FIELDS_NOTES_JWB_FILE);
    }

    private ChooseName uploadBundleAndContinue(SelectWorkflow page, String bundleName)
    {
        page.selectMyComputer();
        assertTrue(page.isUploadContainerVisible());
        assertTrue(page.isNextButtonVisible());
        assertFalse(page.isNextButtonEnabled());
        assertTrue(page.isFileUploadVisible());

        File bundle = ImportTestUtils.getJWBFile(bundleName);
        if (!bundle.exists())
        {
            throw new IllegalStateException("Bundle doesn't exist: " + bundle.getAbsolutePath());
        }
        page.setFile(bundle);
        assertTrue(page.isNextButtonEnabled());

        return page.next();
    }

    private Summary assertMapStatuses(MapStatuses page)
    {
        assertButtons(page);

        assertEquals(asList("Closed (6)", "Open (1)"), page.getOriginalStatusNames());

        ChooseName chooseName = page.back();
        assertEquals("My Workflow", chooseName.getName());
        chooseName.setName("My Workflow Changed");
        page = chooseName.next();

        assertAndChangeStatuses(page, "6", "1", "-1", "3");

        Summary summary = page.next();
        assertCurrentURL(summary.getUrl());

        return summary;
    }

    private WorkflowImported assertSummary(Summary page)
    {
        assertButtons(page);

        assertTrue(page.isPluginWarningShown());
        assertEquals(asList("JIRA Misc Workflow Extensions", "JIRA Suite Utilities", "JIRA Toolkit Plugin"), page.getPluginNames());

        assertTrue(page.isStatusesListPresent());
        assertThat(page.getStatusNames(), IsCollectionContaining.hasItems("CLOSED - 2"));
        assertThat(page.getScreensNames(), IsCollectionContaining.hasItems("screenWithCustomField", "screenWithoutCustomField"));
        assertFalse(page.isNothingToCreateInfoShown());

        assertCustomFields(page);

        MapStatuses mapStatuses = page.browserBack(1);
        assertAndChangeStatuses(mapStatuses, "-1", "3", "6", "1");
        page = mapStatuses.next();

        assertFalse(page.isStatusesListPresent());
        assertFalse(page.isNothingToCreateInfoShown());

        mapStatuses = page.back();
        assertAndChangeStatuses(mapStatuses, "6", "1", "6", "-1");
        page = mapStatuses.next();

        assertTrue(page.isStatusesListPresent());
        assertThat(page.getStatusNames(), IsCollectionContaining.hasItems("OPEN - 2"));

        WorkflowImported workflowImported = page.next();
        assertCurrentURL(workflowImported.getUrl());

        return workflowImported;
    }

    private void assertCustomFields(Summary page)
    {
        assertFalse(page.isCustomFieldWarningShown());
        assertTrue(page.isCustomFieldsTableShown());
        assertEquals(asList("myCascadingSelectField", "myTextField"), page.getCustomFieldNames());

        backdoor.plugins().disablePluginModule("com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect");
        jira.getTester().getDriver().navigate().refresh();

        assertTrue(page.isCustomFieldWarningShown());
        assertEquals(asList("com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect"), page.getCustomFieldWarningTypes());
        assertTrue(page.isCustomFieldsTableShown());
        assertEquals(asList("myTextField"), page.getCustomFieldNames());

        backdoor.plugins().disablePluginModule("com.atlassian.jira.plugin.system.customfieldtypes:textfield");
        jira.getTester().getDriver().navigate().refresh();

        assertTrue(page.isCustomFieldWarningShown());
        assertEquals(asList("com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect", "com.atlassian.jira.plugin.system.customfieldtypes:textfield"), page.getCustomFieldWarningTypes());
        assertFalse(page.isCustomFieldsTableShown());
    }

    private void assertAndChangeStatuses(MapStatuses mapStatuses,
                                         String expectedCloseOptionValue, String expectedOpenOptionValue,
                                         String newCloseOptionValue, String newOpenOptionValue)
    {
        Option selectedClosedOption = mapStatuses.getClosedStatusSelect().getSelected();
        Option selectedOpenOption = mapStatuses.getOpenStatusSelect().getSelected();

        assertEquals(expectedCloseOptionValue, selectedClosedOption.value());
        assertEquals(expectedOpenOptionValue, selectedOpenOption.value());

        changeStatuses(mapStatuses, newCloseOptionValue, newOpenOptionValue);
    }

    private void changeStatuses(MapStatuses mapStatuses, String newCloseOptionValue, String newOpenOptionValue)
    {
        mapStatuses.getClosedStatusSelect().select(Options.value(newCloseOptionValue));
        mapStatuses.getOpenStatusSelect().select(Options.value(newOpenOptionValue));
    }

    private void assertWorkflowImported(WorkflowImported page) throws IOException
    {
        assertTrue(page.isSuccessMessageShown());
        assertEquals("Workflow has been successfully imported", page.getMessageTitle());

        assertTrue(page.isAdditionalConfigurationWarningShown());
        assertEquals(StringUtils.stripEnd(getExpectedTestNotesForImport(), "\n"), page.getNotes());

        page.finish();
        jira.getPageBinder().bind(WorkflowsPage.class);
        assertCurrentURL(getExpectedFinishUrl());

        jira.getTester().getDriver().navigate().back();
        jira.getTester().getDriver().navigate().back();

        SelectWorkflow selectWorkflow = jira.getPageBinder().bind(SelectWorkflow.class);
        assertTrue(getCurrentUrl().contains("notFoundInSession=true"));
        assertTrue(selectWorkflow.isErrorMessageShown());
        assertEquals("Import wizard must be restarted.", selectWorkflow.getMessageParagraph());

        assertNotNull(backdoor.workflow().getWorkflowDetailed("My Workflow Changed"));
    }

    @Override
    protected String getExpectedFinishUrl()
    {
        return new WorkflowsPage().getUrl();
    }
}
