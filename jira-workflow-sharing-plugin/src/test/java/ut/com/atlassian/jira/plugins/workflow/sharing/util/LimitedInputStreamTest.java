package ut.com.atlassian.jira.plugins.workflow.sharing.util;

import com.atlassian.jira.plugins.workflow.sharing.util.LimitedInputStream;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@SuppressWarnings ("ResultOfMethodCallIgnored")
public class LimitedInputStreamTest
{
    @Test
    public void testRead() throws IOException
    {
        final InputStream inputOfSize = getInputOfSize(3);
        LimitedInputStream limited = new LimitedInputStream(inputOfSize, 2);

        assertEquals(0, limited.read());
        assertEquals(1, limited.read());
        try
        {
            limited.read();
            fail("Expected exception.");
        }
        catch (LimitedInputStream.StreamTooBigException tooBigException)
        {
            //good.
        }
    }

    @Test
    public void testReadArray() throws IOException
    {
        final InputStream inputOfSize = getInputOfSize(9);
        LimitedInputStream limited = new LimitedInputStream(inputOfSize, 8);

        byte[] buffer = new byte[5];
        assertEquals(5, limited.read(buffer));
        assertTrue(Arrays.equals(new byte[]{0, 1, 2, 3, 4}, buffer));

        assertEquals(3, limited.read(buffer, 2, 3));
        assertTrue(Arrays.equals(new byte[]{0, 1, 5, 6, 7}, buffer));

        try
        {
            limited.read(buffer, 0, 1);
            fail("Expected exception.");
        }
        catch (LimitedInputStream.StreamTooBigException tooBigException)
        {
            //good.
        }

        try
        {
            limited.read(buffer);
            fail("Expected exception.");
        }
        catch (LimitedInputStream.StreamTooBigException tooBigException)
        {
            //good.
        }
    }

    @Test
    public void testSkip() throws IOException
    {
        final InputStream inputOfSize = getInputOfSize(9);
        LimitedInputStream limited = new LimitedInputStream(inputOfSize, 8);

        assertEquals(7, limited.skip(7));
        assertEquals(7, limited.read());

        try
        {
            limited.skip(1);
            fail("Expected exception.");
        }
        catch (LimitedInputStream.StreamTooBigException tooBigException)
        {
            //good.
        }
    }

    private static InputStream getInputOfSize(int len)
    {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        for (int i = 0; i < len; i++)
        {
            output.write(i);
        }
        return new ByteArrayInputStream(output.toByteArray());
    }
}
