package ut.com.atlassian.jira.plugins.workflow.sharing.utils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;

import com.atlassian.plugin.util.ClassLoaderUtils;

import com.google.common.base.Preconditions;

import org.apache.commons.io.FileUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

public class PluginTestUtils
{

    /**
     * Copies the test plugins to a new temporary directory and returns that directory.
     */
    public File copyTestPluginsToTempDirectory() throws IOException
    {
        File directory = createTemporaryDirectory();
        FileUtils.copyDirectory(getTestPluginsDirectory(), directory);

        // Clean up version control files in case we copied them by mistake.
        FileUtils.deleteDirectory(new File(directory, "CVS"));
        FileUtils.deleteDirectory(new File(directory, ".svn"));

        return directory;
    }

    /**
     * Returns the directory on the classpath where the test plugins live.
     */
    public File getTestPluginsDirectory()
    {
        URL url = ClassLoaderUtils.getResource(TestConstants.TEST_PLUGIN_DIRECTORY, PluginTestUtils.class);
        Preconditions.checkNotNull(url, "Could not load the test plugin directory for the test add-on.");
        return new File(url.getFile());
    }

    public File getPluginFile(String filename)
    {
        return new File(getTestPluginsDirectory(), filename);
    }

    private File createTemporaryDirectory()
    {
        File tempDir = new File(TestConstants.PLUGINS_TEMP_DIRECTORY, "plugins-" + randomString(6));
        FileUtils.deleteQuietly(tempDir);
        tempDir.mkdirs();
        return tempDir;
    }

    public void clean()
    {
        FileUtils.deleteQuietly(TestConstants.PLUGINS_TEMP_DIRECTORY);
    }

    /**
     * Generate a random string of characters - including numbers
     *
     * @param length the length of the string to return
     * @return a random string of characters
     */
    private String randomString(int length)
    {
        StringBuffer b = new StringBuffer(length);

        for (int i = 0; i < length; i++)
        {
            b.append(randomAlpha());
        }

        return b.toString();
    }

    /**
     * Generate a random character from the alphabet - either a-z or A-Z
     *
     * @return a random alphabetic character
     */
    private char randomAlpha()
    {
        int i = (int) (Math.random() * 52);

        if (i > 25)
        { return (char) (97 + i - 26); }
        else
        { return (char) (65 + i); }
    }

    public void uploadPluginViaUPM(File pluginJar) throws Exception
    {
        DefaultHttpClient httpClient = new DefaultHttpClient();

        try
        {
            String fileName = pluginJar.getName();

            String resourceUrl = System.getProperty("baseurl") + TestConstants.UPM_ROOT_RESOURCE;
            URI uri = new URI(resourceUrl);

            HttpHost targetHost = new HttpHost(uri.getHost(), uri.getPort());

            //basic auth
            httpClient.getCredentialsProvider().setCredentials(
                    new AuthScope(uri.getHost(), uri.getPort())
                    , new UsernamePasswordCredentials("admin", "admin")
            );

            AuthCache authCache = new BasicAuthCache();
            // Generate BASIC scheme object and add it to the local
            // auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(targetHost, basicAuth);

            // Add AuthCache to the execution context
            BasicHttpContext localcontext = new BasicHttpContext();
            localcontext.setAttribute(ClientContext.AUTH_CACHE, authCache);


            HttpPost httpPost = new HttpPost(resourceUrl);
            HttpHead httpHead = new HttpHead(resourceUrl);

            MultipartEntity entity = new MultipartEntity();
            entity.addPart("plugin", new FileBody(pluginJar));

            String token = null;
            HttpResponse tokenResponse = httpClient.execute(httpHead, localcontext);
            Header tokenHeader = tokenResponse.getFirstHeader("upm-token");

            if (null != tokenHeader)
            {
                token = tokenHeader.getValue();
            }

            if (null == token)
            {
                throw new Exception("couldn't get token from UPM");
            }

            URIBuilder builder = new URIBuilder(httpPost.getURI());
            builder.setParameter("token", token);
            httpPost.setURI(builder.build());
            httpPost.setEntity(entity);

            HttpResponse postResponse = httpClient.execute(httpPost);
            EntityUtils.consume(postResponse.getEntity());

            //need to wait a few seconds for OSGi to start the plugin
            Thread.sleep(5000);
        }
        finally
        {
            httpClient.getConnectionManager().shutdown();
        }

    }


}
