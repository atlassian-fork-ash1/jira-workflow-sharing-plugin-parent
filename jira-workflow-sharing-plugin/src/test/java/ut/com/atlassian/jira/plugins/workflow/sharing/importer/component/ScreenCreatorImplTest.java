package ut.com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.plugins.workflow.sharing.importer.ValidNameGenerator;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.ScreenCreatorImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.apache.commons.lang.StringUtils.abbreviate;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScreenCreatorImplTest
{
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private FieldScreenManager fieldScreenManager;

    private ScreenCreatorImpl screenCreator;

    @Before
    public void setUp()
    {
        ValidNameGenerator validNameGenerator = new ValidNameGenerator(jiraAuthenticationContext);

        screenCreator = new ScreenCreatorImpl(fieldScreenManager, validNameGenerator);

        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void nameStaysTheSameIfThereIsNoExisting()
    {
        when(fieldScreenManager.screenNameExists(anyString())).thenReturn(false);

        String name = screenCreator.getValidName("No duplicate");

        assertEquals("No duplicate", name);
    }

    @Test
    public void abbreviatesIfLengthIsExceeded()
    {
        when(fieldScreenManager.screenNameExists(anyString())).thenReturn(false);

        String name = screenCreator.getValidName(StringUtils.repeat("a", 256));

        assertEquals(StringUtils.repeat("a", 252) + "...", name);
    }

    @Test
    public void addsPrefixOfTwoIfThereIsExisting()
    {
        when(fieldScreenManager.screenNameExists(eq("Existing"))).thenReturn(true);
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");
        when(i18nHelper.getText("wfshare.import.copy", "Existing", " - 2")).thenReturn("Existing - 2");

        String name = screenCreator.getValidName("Existing");

        assertEquals("Existing - 2", name);
    }

    @Test
    public void addsPrefixOfThreeOfIfThereIsExistingAndWithPrefixOfTwo()
    {
        when(fieldScreenManager.screenNameExists(eq("Existing"))).thenReturn(true);
        when(fieldScreenManager.screenNameExists(eq("Existing - 2"))).thenReturn(true);
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");
        when(i18nHelper.getText("wfshare.import.copy.prefix", "3")).thenReturn(" - 3");
        when(i18nHelper.getText("wfshare.import.copy", "Existing", " - 2")).thenReturn("Existing - 2");
        when(i18nHelper.getText("wfshare.import.copy", "Existing", " - 3")).thenReturn("Existing - 3");

        String name = screenCreator.getValidName("Existing");

        assertEquals("Existing - 3", name);
    }

    @Test
    public void addsPrefixOfTwoAndAbbreviatesIfThereIsExistingAndLengthIsExceeded()
    {
        String existingName = StringUtils.repeat("a", 255);

        when(fieldScreenManager.screenNameExists(eq(existingName))).thenReturn(true);
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");

        String abbreviatedName = abbreviate(existingName, 251);
        when(i18nHelper.getText("wfshare.import.copy", abbreviatedName, " - 2")).thenReturn(abbreviatedName + " - 2");

        String name = screenCreator.getValidName(existingName);

        assertEquals(StringUtils.repeat("a", 248) + "... - 2", name);
    }

    @Test
    public void addsPrefixOfThreeAndAbbreviatesIfThereIsExistingAndWithPrefixOfTwoAndLengthIsExceeded()
    {
        String existingName = StringUtils.repeat("a", 255);
        String existingWithPrefixOfTwo= StringUtils.repeat("a", 248) + "... - 2";

        when(fieldScreenManager.screenNameExists(eq(existingName))).thenReturn(true);
        when(fieldScreenManager.screenNameExists(eq(existingWithPrefixOfTwo))).thenReturn(true);
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");
        when(i18nHelper.getText("wfshare.import.copy.prefix", "3")).thenReturn(" - 3");

        String abbreviatedName = abbreviate(existingName, 251);
        when(i18nHelper.getText("wfshare.import.copy", abbreviatedName, " - 2")).thenReturn(abbreviatedName + " - 2");
        when(i18nHelper.getText("wfshare.import.copy", abbreviatedName, " - 3")).thenReturn(abbreviatedName + " - 3");

        String name = screenCreator.getValidName(existingName);

        assertEquals(StringUtils.repeat("a", 248) + "... - 3", name);
    }
}
