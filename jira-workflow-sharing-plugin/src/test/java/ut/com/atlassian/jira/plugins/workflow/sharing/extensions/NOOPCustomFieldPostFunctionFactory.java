package ut.com.atlassian.jira.plugins.workflow.sharing.extensions;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NOOPCustomFieldPostFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory
{
    public static final String PARAM_FIELD_ID = "fieldId";
    public static final String TARGET_FIELD_NAME = "field.name";
    
    private final CustomFieldManager customFieldManager;

    public NOOPCustomFieldPostFunctionFactory(CustomFieldManager customFieldManager)
    {
        this.customFieldManager = customFieldManager;
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> stringObjectMap)
    {
        List<CustomField> customFields = customFieldManager.getCustomFieldObjects();

        stringObjectMap.put("fields", customFields);
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> stringObjectMap, AbstractDescriptor abstractDescriptor)
    {
        getVelocityParamsForInput(stringObjectMap);

        if (!(abstractDescriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        FunctionDescriptor functionDescriptor = (FunctionDescriptor) abstractDescriptor;

        stringObjectMap.put(PARAM_FIELD_ID, functionDescriptor.getArgs().get(TARGET_FIELD_NAME));

    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> stringObjectMap, AbstractDescriptor descriptor)
    {
        if (!(descriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        } else {
            FunctionDescriptor functionDescriptor = (FunctionDescriptor) descriptor;

            String fieldName = (String) functionDescriptor.getArgs().get("field.name");

            stringObjectMap.put(
                    PARAM_FIELD_ID,
                    customFieldManager.getCustomFieldObject(fieldName).getNameKey()
            );
            
            return;
        }
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> stringObjectMap)
    {
        Map<String, String> params = new HashMap<String, String>();

        String fieldId = extractSingleParam(stringObjectMap, PARAM_FIELD_ID);
        params.put(TARGET_FIELD_NAME, fieldId);

        return params;
    }
}
