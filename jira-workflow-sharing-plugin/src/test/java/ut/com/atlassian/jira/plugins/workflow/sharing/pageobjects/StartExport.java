package ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class StartExport extends WizardPage<StartExport, AddNotes>
{
    private final String workflowName;

    public StartExport(String workflowName)
    {
        this.workflowName = workflowName;
    }

    @Override
    public String getUrl()
    {
        try
        {
            return "/plugins/servlet/wfshare-export?workflowMode=live&workflowName=" + encode(workflowName);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
    }

    private String encode(String value) throws UnsupportedEncodingException
    {
        return URLEncoder.encode(value, "UTF-8");
    }

    @Override
    protected Class<AddNotes> getNextClass()
    {
        return AddNotes.class;
    }

    @Override
    protected Class<StartExport> getBackCass()
    {
        return StartExport.class;
    }
}
